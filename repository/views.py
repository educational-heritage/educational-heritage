"""repository app views."""

from django.shortcuts import render

from repository.models import Item


def index(request):
    """Home page view."""
    items = Item.objects.all()
    return render(request, 'repository/index.html', {'items': items})
