"""repository app unit tests."""

from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase

from repository.views import index


class HomePageTest(TestCase):
    """Home Page Test Suite."""

    def test_root_url_resolves_to_home_page_view(self):
        """"Test Case: Home Page."""
        found = resolve('/repository/')
        self.assertEqual(found.func, index)

    def test_home_page_returns_correct_html(self):
        """Test Case: Home Page is using correct template."""
        request = HttpRequest()
        response = index(request)

        expected_html = render_to_string('repository/index.html')

        self.assertEqual(response.content.decode(), expected_html)
