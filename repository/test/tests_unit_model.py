"""Repository model tests."""

from django.test import TestCase

from repository.models import Item


class ItemModelTest(TestCase):
    """Item model tests suite."""

    def test_saving_and_retrieving_items(self):
        """Test Case: saving and retrieving items."""
        first_item = Item()
        first_item.title = 'The first (ever) list item'
        first_item.save()

        second_item = Item()
        second_item.title = 'Item the second'
        second_item.save()

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.title, 'The first (ever) list item')
        self.assertEqual(second_saved_item.title, 'Item the second')
