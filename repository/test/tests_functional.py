"""Repository funtional tests suite."""

import unittest

from django.test import tag
from pyvirtualdisplay import Display
from selenium import webdriver


@tag('functional')
class RepositoryTest(unittest.TestCase):
    """Repository Test Suite."""

    def setUp(self):
        """Test Case set up."""
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()

        self.browser = webdriver.Chrome()

    def tearDown(self):
        """Test Case tear down."""
        self.browser.close()
        self.display.stop()

    def test_list_all_repository_items(self):
        """Test Case: visit repository home page."""
        # User goes to checkout the Open Educational Resources repository
        self.browser.get('https://localhost:8000/repository/')
        # print(self.browser.page_source)

        # User notices the page title and header mention resources lists
        self.assertIn('Recursos Educativos', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Recursos Educativos Abiertos', header_text)

        # Now the page lists:
        # "1: Libro de ejercicios de multiplicar" as an item in a resources list table
        rows = self.browser.find_elements_by_tag_name('h3')
        self.assertTrue(
            any(row.text == 'Libro de ejercicios de multiplicar' for row in rows),
            "No resource item did appear\n"
        )
