"""Repository urls."""

from django.conf.urls import url

from . import views

app_name = 'repository'

urlpatterns = [
    # /repository/
    url(r'^$', views.index, name='index'),
]
