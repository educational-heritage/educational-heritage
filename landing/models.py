"""Landing models."""
from __future__ import unicode_literals

import uuid

from django.db import models


class Subscriber(models.Model):
    """Subscriber model."""

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(unique=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Return email."""
        return self.email
