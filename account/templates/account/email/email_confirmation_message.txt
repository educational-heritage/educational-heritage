{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Bienvenido a {{ site_name }}!

Hes recibido este correo desde {{ site_domain }} para conectar tu cuenta.

Para confirmar que es correcto, vete a {{ activate_url }}

{% endblocktrans %}{% endautoescape %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Gracias desde {{ site_name }}!
{{ site_domain }}{% endblocktrans %}
